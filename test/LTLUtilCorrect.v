Require Import List.

Require Import ltac_utils.

Require Import IO.intf.World.

Require Import LTLUtil.
Require Import LTLUtilFacts.

Module LTLUtilCorrect
       (w : World)
.

Module ltlu := LTLUtil w.
Import ltlu.
Module ltluf := LTLUtilFacts w.
Import ltluf.

Lemma ever_corr2 :
  forall
    (event : w.msg -> Prop)
    l,
    ever_happened event l ->
    (exists m, event m /\ In m l).
Proof.
  induction l.
  { intro. destruct H as [ s [ t' [ ppf H ] ] ]. destruct s; [ destruct t'; [ contradiction (proj1 H) | discriminate ppf ] | discriminate ppf ]. }
  rewrite ever_happened_unfold.
  intros eal.
  destruct eal.
  - exists a. split; [ exact H | ]. simpl. left. reflexivity.
  - assert (IHl' := IHl H).
    destruct IHl'. exists x. split; [ exact (proj1 H0) | ].
    simpl. right. exact (proj2 H0).
Qed.

Lemma ever_corr4 :
  forall
    (event : w.msg -> Prop)
    (edec : forall m, {event m}+{~event m})
    l,
    (exists m, event m /\ In m l) ->
    ever_happened event l.
Proof.
  induction l.
  { simpl. intro. destruct H. contradiction (proj2 H). }
  intros.
  rewrite ever_happened_unfold.
  destruct H. destruct H. destruct H0.
  - subst x. left. exact H.
  - right. apply IHl. exists x. exact (conj H H0).
Qed.

Lemma ever_corr :
  forall
    (event : w.msg -> Prop)
    (edec : forall m, {event m}+{~event m})
    l,
    (exists m, event m /\ In m l) <->
    ever_happened event l.
Proof.
  intros.
  exact (conj (ever_corr4 _ edec _) (ever_corr2 _ _)).
Qed.

Lemma ever_corr3 :
  forall
    (event : w.msg -> Prop)
    l,
    Forall
      (fun m => ~event m)
      l ->
    ~ever_happened event l.
Proof.
  intros. intro. rewrite Forall_forall in *.
  assert (Hx := ever_corr2 _ _ H0). clear H0.
  destruct Hx as [ m [ em mil ] ].
  exact (H m mil em).
Qed.

End LTLUtilCorrect.
