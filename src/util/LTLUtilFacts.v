Require Import List.

Require Import ltac_utils.
Require Import ltac_sublist.
Require Import sublist.
Require Import sublist_facts.
Require Import exists_utils.

Require Import IO.intf.World.

Require Import LTLUtil.

Module LTLUtilFacts
       (w : World)
.

Module ltlu := LTLUtil w.
Export ltlu.
Import ltlu.

Lemma strictly_after_is_after :
  forall state1 state2 l,
    strictly_after state1 state2 l -> after state1 state2 l.
Proof.
  destruct l.

  - unfold strictly_after. unfold after. intro.
    destruct H as [ s [ t [ ppf postpf ] ] ]. exists nil. exists nil. split; [ reflexivity | ].
    destruct postpf. split; [ | exact H0 ].
    unfold ever_was in *.
    assert (s = nil). { destruct s; [ reflexivity | discriminate ppf ]. } subst s.
    simpl in ppf. subst t.
    destruct H as [ s [ t [ ppf [ tnotnil state1t ] ] ] ].
    assert (s = nil). { destruct s; [ reflexivity | discriminate ppf ]. } subst s.
    simpl in ppf. subst t. elim tnotnil. intros. elim H. reflexivity.

  - unfold strictly_after in *. unfold after in *. intro.
    destruct H as [ s [ t [ ppf [ everwasst1t everwasst2al ] ] ] ].
    exists s. exists t. split; [ exact ppf | ].
    split; [ | exact everwasst2al ].
    unfold ever_was in *. destruct everwasst1t as [ s' [ t' [ ppf' [ t'nn st1t' ] ] ] ].
    subst t. exists s'. exists t'. split; [ reflexivity | ].
    split; [ exact (proj2 t'nn) | ].
    intros. exact I.
Qed.

Lemma tillnow_msg_unfold :
  forall a oof s e t,
    tillnow_msg oof s e t ->
    tillnow_msg_keeps oof s e a ->
    tillnow_msg oof s e (a::t).
Proof.
  intros ? ? ? ? ? tk akeeps. unfold tillnow_msg, tillnow_msg_keeps, tillnow in *.
  destruct tk as [ keepseg [ startseg [ ppf tk ] ] ].
  destruct tk as [ startpf keeppf ].
  subst t. destruct startseg; [ contradiction startpf | ].
  rename m into startelem.
  exists (a::keepseg). exists (startelem::startseg).
  split; [ reflexivity | ].
  split; [ exact startpf | ].
  intros. destruct H as [ s'' H ]. destruct s''.
  - destruct s'; [ discriminate H | ].
    injit H. simpl. exact akeeps.
  - simpl in H. injit H.
    assert (keeppf' := keeppf _ (ex_intro _ _ eq_refl)). clear keeppf.
    exact keeppf'.
Qed.

Lemma ever_happened_unfold :
  forall
    (event : w.msg -> Prop)
    l a,
    ever_happened event (a::l) <->
    (event a \/ ever_happened event l).
Proof.
  split.

  {
    intros. unfold ever_happened in H. unfold tillnow_msg in H. unfold tillnow in H.
    destruct H as [ s [ t' [ ppf H ] ] ].
    destruct H.
    destruct t'; [ contradiction H | ].
    destruct s; [ left | right ].
    { injit ppf. exact H. }
    unfold ever_happened. unfold tillnow_msg. unfold tillnow.
    exists s. exists (m::t'). injit ppf.
    split; [ reflexivity | ].
    split; [ exact H | ].
    intros. apply H0. destruct H1 as [ s'' sdef ]. subst s. exists (a::s''). reflexivity.
  }

  {
    intros.
    unfold ever_happened. unfold tillnow_msg. unfold tillnow.
    destruct H.
    - exists nil. exists (a::l).
      split; [ reflexivity | ].
      split; [ exact H | ].
      intros. destruct H0 as [ s'' H0 ]. destruct s''; [ | discriminate H0 ]. destruct s'; [ | discriminate H0 ].
      simpl. compute. right. right. exact H.
    - unfold ever_happened in H. unfold tillnow_msg in H. unfold tillnow in H.
      destruct H as [ s [ t' [ ppf [ eventpf keeppf ] ] ] ].
      subst l. destruct t'; [ contradiction eventpf | ].
      exists (a::s). exists (m::t').
      split; [ reflexivity | ].
      split; [ exact eventpf | ].
      intros. destruct H as [ s'' H ]. destruct s''.
      + simpl in H. subst s'. simpl. compute. right. left. exact I.
      + apply keeppf.
        injit H. exists s''. reflexivity.
  }
Qed.

Lemma tillnow_nil :
  forall
    (starts keeps : list w.msg -> Prop),
    tillnow starts keeps nil <->
    (starts nil /\ keeps nil).
Proof.
  split.
  {
    compute. intros. destruct H as [ s [ t' [ ppf [ eventpf keeppf ] ] ] ].
    destruct s; [ | discriminate ppf ].
    subst t'.
    assert (keeppf' := keeppf nil (ex_intro _ nil eq_refl)). simpl in keeppf'.
    exact (conj eventpf keeppf').
  }
  {
    intros. compute. exists nil. exists nil.
    split; [ reflexivity | ].
    split; [ exact (proj1 H) | ].
    intros. destruct H0 as [ s'' H0 ]. destruct s''; [ | discriminate H0 ]. subst s'.
    exact (proj2 H).
  }
Qed.

Lemma tillnow_and_starts_then_keeps :
  forall
    (starts keeps : list w.msg -> Prop)
    t,
    tillnow starts keeps t ->
    starts t ->
    keeps t.
Proof.
  intros ? ? ? H.
  unfold tillnow in H. destruct H as [ s [ t' [ ppf [ startpf keeppf ] ] ] ].
  subst t.
  intros. apply keeppf.
  exists nil. reflexivity.
Qed.

Lemma tillnow_unfold :
  forall
    (starts keeps : list w.msg -> Prop)
    m t,
    tillnow starts keeps (m::t) ->
    (starts (m::t) \/ (tillnow starts keeps t /\ keeps (m::t))).
Proof.
  unfold tillnow. intros. destruct H as [ s [ t' [ ppf [ startpf keeppf ] ] ] ].
  destruct s.
  + left. simpl in ppf. subst t'. exact startpf.
  + right. injit ppf.
    split.
    * exists s. exists t'.
      split; [ reflexivity | ].
      split; [ exact startpf | ].
      intros. destruct H as [ s'' sdef ]. subst s.
      apply keeppf.
      exists (m::s''). reflexivity.
    * rewrite app_comm_cons. apply keeppf.
      exists nil. reflexivity.
Qed.

Lemma tillnow_unfold_seg :
  forall
    (starts keeps : list w.msg -> Prop)
    t,
    tillnow starts keeps t ->
    (exists s t', s ++ t' = t /\ starts t' /\ (forall s', postfix_of s' s -> keeps (s' ++ t'))).
Proof.
  induction t.

  - intros. unfold tillnow in H. destruct H. destruct H. destruct H.
    destruct x; [ destruct x0; [ | discriminate H ] | discriminate H ].
    exists nil. exists nil.
    split; [ reflexivity | ].
    split; [ solve [ apply H0 ] | ].
    intros. destruct H1. destruct x; [ destruct s'; [ | discriminate H1 ] | discriminate H1 ].
    destruct H0. apply H2. exists nil. reflexivity.

  - intros. destruct (tillnow_unfold _ _ _ _ H).
    + exists nil. exists (a::t).
      split; [ reflexivity | ].
      split; [ assumption | ].
      intros. destruct H1. destruct x; [ destruct s'; [ | discriminate H1 ] | discriminate H1 ].
      simpl. assert (Hx := tillnow_and_starts_then_keeps _ _ _ H H0). exact Hx.
    + destruct H0. assert (IHt' := IHt H0); clear IHt.
      destruct IHt'. destruct H2.
      explode H2. subst t.
      exists (a::x). exists x0.
      split; [ reflexivity | ].
      split; [ assumption | ].
      intros. destruct (postfix_of_cons _ _ _ H2).
      * apply H4. exact H5.
      * subst s'. simpl. exact H1.
Qed.

Lemma tillnow_fold :
  forall
    (starts keeps : list w.msg -> Prop)
    a l
    (skconsistent : starts (a::l) -> keeps (a::l)),
    (starts (a::l) \/ (tillnow starts keeps l /\ keeps (a::l))) ->
    tillnow starts keeps (a::l).
Proof.
  intros. destruct H.
  - unfold tillnow. exists nil. exists (a::l).
    split; [ reflexivity | ].
    split; [ exact H | ].
    intros. destruct H0 as [ s'' H0 ]. destruct s''; [ | discriminate H0 ]. destruct s'; [ | discriminate H0 ].
    apply skconsistent. exact H.
  - unfold tillnow in *. destruct H.
    destruct H as [ s [ t' [ ppf [ startspf keepspf ] ] ] ].
    subst l. exists (a::s). exists t'.
    split; [ reflexivity | ].
    split; [ exact startspf | ].
    intros. destruct H as [ s'' H ]. destruct s''; [ destruct s'; [ discriminate H | ] | ].
    + injit H. simpl. exact H0.
    + apply keepspf.
      simpl in H. injit H. exact (ex_intro _ _ eq_refl).
Qed.

Lemma tillnow_app :
  forall
    (starts keeps : list w.msg -> Prop)
    s t,
    tillnow starts keeps t ->
    (forall s', sublist.postfix_of s' s -> keeps (s' ++ t)) ->
    tillnow starts keeps (s ++ t).
Proof.
  intros ? ? s_new ? tok sok.

  unfold tillnow in *.
  destruct tok as [ s_old [ t' [ ppf tok ] ] ]. subst t.
  exists (s_new ++ s_old). exists t'.
  split; [ rewrite app_assoc; reflexivity | ].
  split; [ exact (proj1 tok) | ].
  intros ? s'def.
  assert (sok' := sok s').
  destruct tok as [ _ tok ].
  assert (Hx := forall_postfix_of_join _ _ _ _ tok sok).
  apply (Hx _ s'def).
Qed.

Definition Pcov (P : Prop -> Prop) : Prop :=
  forall
    Q Q' : Prop,
    P Q ->
    (Q -> Q') ->
    P Q'.

Definition Pcontrav (P : Prop -> Prop) : Prop :=
  forall
    Q Q' : Prop,
    P Q ->
    (Q' -> Q) ->
    P Q'.

Lemma and_cov_1 :
  forall
    P : Prop,
    Pcov (and P).
Proof.
  unfold Pcov. intros. split; [ apply H | ]. apply H0. apply H.
Qed.

Lemma not_contra :
    Pcontrav not.
Proof.
  firstorder.
Qed.

(** If the start condition implies some condition [start'], then the range designated by [start'] and [end] also is in effect. *)
Lemma tillnow_starts_cov :
  forall
    (starts starts' keeps : list w.msg -> Prop)
    t,
    tillnow starts keeps t ->
    (forall t, starts t -> starts' t) ->
    tillnow starts' keeps t.
Proof.
  intros ? ? ? ? origpf impl. induction t.
  { rewrite tillnow_nil in *. split; [ | exact (proj2 origpf) ]. apply impl. exact (proj1 origpf). }
  destruct (tillnow_unfold _ _ _ _ origpf) as [ astarts | akeeps ].
  - assert (startsthenkeeps := tillnow_and_starts_then_keeps _ _ _ origpf).
    unfold tillnow. exists nil. exists (a::t).
    split; [ reflexivity | ].
    split; [ exact (impl _ astarts) | ].
    intros ? snil. destruct snil as [ s'' snil ]. destruct s''; [ | discriminate snil ]. destruct s'; [ | discriminate snil ].
    apply startsthenkeeps. exact astarts.
  - clear origpf. assert (IHt' := IHt (proj1 akeeps)). clear IHt. destruct akeeps as [ _ akeeps ].
    unfold tillnow in IHt'. destruct IHt' as [ s [ t' [ ppf [ startpf keeppf ] ] ] ].
    subst t. unfold tillnow. exists (a::s). exists t'.
    split; [ reflexivity | ].
    split; [ exact startpf | ].
    intros. destruct H as [ s'' H ]. destruct s''; [ destruct s' | ].
    + discriminate H.
    + injit H. simpl. exact akeeps.
    + injit H. apply keeppf. exists s''. reflexivity.
Qed.

(** Weakening the [keeps] condition does not invalidate the [tillnow] property of a trace. *)
Lemma tillnow_keeps_cov :
  forall
    (starts keeps keeps' : list w.msg -> Prop)
    t,
    tillnow starts keeps t ->
    (forall t, keeps t -> keeps' t) ->
    tillnow starts keeps' t.
Proof.
  intros ? ? ? ? origpf impl. induction t.
  { rewrite tillnow_nil in *. exact (conj (proj1 origpf) (impl _ (proj2 origpf))). }
  unfold tillnow.
  destruct (tillnow_unfold _ _ _ _ origpf) as [ astarts | akeeps ].
  - assert (startsthenkeeps := tillnow_and_starts_then_keeps _ _ _ origpf).
    exists nil. exists (a::t).
    split; [ reflexivity | ].
    split; [ exact astarts | ].
    intros. destruct H as [ s'' H ]. destruct s''; [ | discriminate H ]. destruct s'; [ | discriminate H ].
    apply impl. apply startsthenkeeps. exact astarts.
  - clear origpf. assert (IHl' := IHt (proj1 akeeps)). clear IHt. destruct akeeps as [ _ akeeps ].
    unfold tillnow in IHl'. destruct IHl' as [ s [ t' [ ppf [ startspf keepspf ] ] ] ].
    subst t. exists (a::s). exists t'.
    split; [ reflexivity | ].
    split; [ exact startspf | ].
    intros. destruct H as [ s'' H ]. destruct s''; [ destruct s'; [ discriminate H | ] | ].
    + injit H. apply impl. exact akeeps.
    + injit H. apply keepspf. exists s''. reflexivity.
Qed.

(** Weakening either [starts] or [keeps] widens the coverage of [tillnow]. *)
Lemma tillnow_cov :
  forall
    (starts starts' keeps keeps' : list w.msg -> Prop)
    t,
    tillnow starts keeps t ->
    (forall t, starts t -> starts' t) ->
    (forall t, keeps t -> keeps' t) ->
    tillnow starts' keeps' t.
Proof.
  intros.
  assert (sc := tillnow_starts_cov starts starts' keeps t H H0).
  assert (kc := tillnow_keeps_cov starts' keeps keeps' t sc H1).
  exact kc.
Qed.

(** If the start condition implies some condition [start'], then the range designated by [start'] and [end] also is in effect. *)
Lemma tillnow_msg_starts_cov :
  forall
    (known starts starts' ends : w.msg -> Prop)
    t,
    tillnow_msg known starts ends t ->
    (forall m, starts m -> starts' m) ->
    tillnow_msg known starts' ends t.
Proof.
  intros ? ? ? ? ? orig impl.
  refine (tillnow_cov _ _ _ _ t orig _ _).
  - intros ? H. destruct t0; [ contradiction H | ]. apply impl. exact H.
  - intros ? H. destruct t0; [ exact I | ].
    unfold tillnow_msg_keeps in *. destruct H as [ H | [ H | H ] ].
    + left. exact H.
    + right; left. exact H.
    + right; right. apply impl. exact H.
Qed.

(** If the condition [end'] implies the end condition, then the range designated by start and [end'] also is in effect. *)
Lemma tillnow_msg_ends_contrav :
  forall
    (known starts ends ends' : w.msg -> Prop)
    t,
    tillnow_msg known starts ends t ->
    (forall m, ends' m -> ends m) ->
    tillnow_msg known starts ends' t.
Proof.
  intros ? ? ? ? ? orig impl.
  refine (tillnow_cov _ _ _ _ t orig _ _).
  - intros ? ?. destruct t0; [ contradiction H | ]. exact H.
  - intros ? ?. destruct t0; [ exact I | ].
    unfold tillnow_msg_keeps in *. destruct H as [ H | [ H | H ] ].
    + left. intro. elim H. apply impl. exact H0.
    + right; left. exact H.
    + right; right. exact H.
Qed.

Lemma ever_happened_weaken :
  forall
    (P Q : w.msg -> Prop)
    (mprop_impl : forall m : w.msg, P m -> Q m)
    (t : list w.msg),
    (ever_happened P t -> ever_happened Q t).
Proof.
  intros. unfold ever_happened in *.
  exact (tillnow_msg_starts_cov _ _ _ _ t H mprop_impl).
Qed.

Lemma tillnow_or_dist_start_r :
  forall
    starts starts' keeps t,
    (tillnow starts keeps t \/ tillnow starts' keeps t) ->
    (tillnow (fun t => starts t \/ starts' t) keeps t).
Proof.
  intros. destruct H.
  - apply tillnow_starts_cov with starts; auto.
  - apply tillnow_starts_cov with starts'; auto.
Qed.

Lemma exists_dist :
  forall {A P Q},
    (exists a : A, P a \/ Q a) -> (exists a, P a) \/ (exists a, Q a).
Proof. firstorder. Qed.

Lemma and_dist :
  forall P Q R,
    (P /\ (Q \/ R)) -> (P /\ Q) \/ (P /\ R).
Proof. firstorder. Qed.
Lemma and_dist2 :
  forall P Q R,
    (P /\ (Q \/ R)) -> (Q /\ P) \/ (R /\ P).
Proof. firstorder. Qed.

Lemma tillnow_or_dist_start_l :
  forall
    starts starts' keeps t,
    (tillnow (fun t => starts t \/ starts' t) keeps t) ->
    (tillnow starts keeps t \/ tillnow starts' keeps t).
Proof.
  (* firstorder. *)
  intros; unfold tillnow in *;
  destruct H; destruct H; destruct H; subst t; destruct H0;
  apply exists_dist; eexists; apply exists_dist; eexists;
  apply and_dist; split; [ reflexivity | ];
  apply and_dist2; split; [ assumption | ];
  assumption.
Qed.

Lemma tillnow_or_dist_start :
  forall
    starts starts' keeps t,
    (tillnow (fun t => starts t \/ starts' t) keeps t) <->
    (tillnow starts keeps t \/ tillnow starts' keeps t).
Proof.
  intros; split; intro;
    [ apply tillnow_or_dist_start_l
    | apply tillnow_or_dist_start_r
    ]; auto.
Qed.

Lemma tillnow_or_dist_keeps_r :
  forall
    starts keeps keeps' t,
    (tillnow starts keeps t \/ tillnow starts keeps' t) ->
    (tillnow starts (fun t => keeps t \/ keeps' t) t).
Proof.
  intros. destruct H.
  - apply tillnow_keeps_cov with keeps; auto.
  - apply tillnow_keeps_cov with keeps'; auto.
Qed.

Lemma tillnow_and_dist_start_l :
  forall
    starts starts' keeps t,
    (tillnow (fun t => starts t /\ starts' t) keeps t) ->
    (tillnow starts keeps t /\ tillnow starts' keeps t).
Proof.
  firstorder.
Qed.

Lemma tillnow_and_dist :
  forall
    starts starts' keeps t,
    (tillnow starts keeps t /\ tillnow starts' keeps t) ->
    (tillnow (fun t => tillnow starts keeps t /\ starts' t) keeps t) \/
    (tillnow (fun t => starts t /\ tillnow starts' keeps t) keeps t).
Proof.
  intros. destruct H; unfold tillnow in *.
  destruct H as [ ? [ ? [ ? ? ] ] ].
  destruct H0 as [ ? [ ? [ ? ? ] ] ].
  subst t.
  destruct (prefix_of_app_elim3way H0) as [ x1short | [ x1eqx | x1long ] ].
  - destruct x1short as [ ? [ nn [ xd ? ] ] ]. subst x. subst x2.
    left. clear H0. exists x1. exists (x3 ++ x0).
    split; [ rewrite app_assoc; reflexivity | ].
    split; [ | apply H2 ].
    split; [ | apply H2 ].
    eexists. eexists. split; [ reflexivity | ].
    split; [ apply H1 | ].
    intros. apply (proj2 H1). apply postfix_of_app. assumption.
  - destruct x1eqx. subst. clear H0.
    left. eexists. eexists. split; [ reflexivity | ].
    split; [ | apply H1 ].
    split; [ | apply H2 ].
    exists nil. eexists. split; [ reflexivity | ].
    split; [ apply H1 | ].
    intros. apply H1. assert (Hx := postfix_of_nil _ H). subst. exists x. rewrite app_nil_r. reflexivity.
  - destruct x1long. destruct H. destruct H3. subst x1. subst x0.
    clear H0.
    right (* [starts'] is shorter (i.e. started earlier), which corresponds to the right case in the original lemma's consequence *).
    eexists. eexists. split; [ reflexivity | ].
    split; [ split | ];
      [ solve [ apply H1 ]
      | idtac
      | solve [ apply H1 ]
      ].
    eexists. eexists. split; [ reflexivity | ].
    split; [ apply H2 | ].
    intros. destruct H2. apply H3. destruct H0. subst x3. eexists. rewrite app_assoc. reflexivity.
Qed.

Lemma tillnow_flatten_l :
  forall
    starts keeps t,
    tillnow (tillnow starts keeps) keeps t ->
    tillnow starts keeps t.
Proof.
  unfold tillnow. intros.
  destruct H as [ ? [ ? [ ? [ ? ? ] ] ] ]. subst t.
  destruct H0 as [ ? [ ? [ ? ? ] ] ]. subst x0.

  exists (x ++ x1). exists x2.
  split; [ rewrite app_assoc; reflexivity | ].
  split; [ apply H0 | ].
  intros. destruct H.
  destruct H0 as [ _ ? ].
  pfelim H.
  - subst x. subst s'. clear H.
    rewrite <- app_assoc. apply H1. exists x0. reflexivity.
  - subst. clear H.
    assert (Hx := H1 nil). simpl in Hx. apply Hx.
    exists x. rewrite app_nil_r. reflexivity.
  - apply H0. exists x3. assumption.
Qed.

Lemma tillnow_flatten_r :
  forall
    starts keeps t,
    tillnow starts keeps t ->
    tillnow (tillnow starts keeps) keeps t.
Proof.
  unfold tillnow. intros.
  destruct H as [ ? [ ? [ ? [ ? ? ] ] ] ]. subst t.

  eexists. eexists. split; [ reflexivity | ].
  split; [ | exact H1 ].

  exists nil. eexists. split; [ reflexivity | ].
  split; [ exact H0 | ].

  intros. apply H1. destruct H. destruct (app_eq_nil _ _ H). subst.
  exists x. rewrite app_nil_r. reflexivity.
Qed.

Lemma tillnow_flatten :
  forall
    starts keeps t,
    tillnow (tillnow starts keeps) keeps t <->
    tillnow starts keeps t.
Proof.
  intros; split.
  - apply tillnow_flatten_l.
  - apply tillnow_flatten_r.
Qed.

Lemma tillnow_exists_comm_starts :
  forall
    A (starts : A -> list w.msg -> Prop) keeps t,
    (exists a, ltl.tillnow (starts a) keeps t) <->
    (ltl.tillnow (fun t => exists a, starts a t) keeps t).
Proof.
  clear. intros. split; intro.
  - destruct H. destruct H as [ ? [ ? [ ? ? ]]]. subst t.
    destruct H0. exists x0. exists x1. split; [ reflexivity | ].
    split.
    + exists x. assumption.
    + intros. apply H0. assumption.
  - destruct H as [ ? [ ? [ ? ? ] ] ]. subst t.
    destruct H0. destruct H. exists x1. exists x. exists x0. split; [ reflexivity | ].
    split; assumption.
Qed.

Lemma tillnow_exists_keeps :
  forall
    starts A (keeps : A -> list w.msg -> Prop) t,
    (exists a, ltl.tillnow starts (keeps a) t) (*<*)->
    (ltl.tillnow starts (fun t => exists a, keeps a t) t).
Proof.
  clear. intros.
  - destruct H. destruct H as [ ? [ ? [ ? ? ]]]. subst t.
    destruct H0. exists x0. exists x1. split; [ reflexivity | ].
    split.
    + assumption.
    + intros. exists x. apply H0. assumption.
  (*- destruct H as [ ? [ ? [ ? ? ] ] ]. subst t.
    destruct H0. unfold tillnow.
    rewrite exists_flip. exists x.
    rewrite exists_flip. exists x0.
    rewrite exists_indep. split; [ reflexivity | ].
    rewrite exists_indep. split; [ assumption | ].
    apply forall_exists.
    intros. specialize H0 with b.
    apply forall_exists. assumption.*)
Qed.


Lemma tillnow_sound_if :
  forall
    s k t,
    ltl.tillnow s k t ->
    (forall t, s t -> ~k t) ->
    False.
Proof.
  intros. unfold tillnow in H. destruct H as [ ? [ ? [ ? ? ] ] ].
  subst. destruct H1. assert (H0 := H0 _ H).
  elim H0. specialize H1 with nil. apply H1. exists x. rewrite app_nil_r. reflexivity.
Qed.

Definition keeps_after :=
  forall
    starts keeps t,
    tillnow starts (fun t => keeps t \/ starts t) t.

End LTLUtilFacts.
