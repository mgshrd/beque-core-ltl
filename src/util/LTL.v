Require Import sublist.

Require Import IO.intf.World.

(** * Temporal logic primitives *)
Module LTL
       (w : World)
.

(* TODO check for LTL until *)
(** Property describing that in the trace
- [starts] has happened,
- all subtraces since [starts] conform to [keeps]

If [starts] does not imply [keeps], this is never true. I.e. multiple [starts] must be allowed. *)
Definition tillnow
           (starts keeps : list w.msg -> Prop) :
  list w.msg -> Prop
  := fun t =>
       exists s t',
         (s ++ t')%list = t /\ (*=== postfix_of t' t, we need [s] exposed for the [keeps] requirement below *)
         starts t' /\
         forall s',
           postfix_of s' s ->
           keeps (s' ++ t')%list.

End LTL.
