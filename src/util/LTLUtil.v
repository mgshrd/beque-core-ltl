Require Import IO.intf.World.

Require Import LTL.

Module LTLUtil
       (w : World)
.

Module ltl := LTL w.
Export ltl.
Import ltl.

Definition tillnow_msg_keeps
           (oof starter ender : w.msg -> Prop) :
  w.msg -> Prop
  := fun m => ~ender m \/ oof m \/ starter m.

Definition tillnow_msg
           oof sx ex
  := tillnow
       (fun t => match t with
                   | nil => False
                   | (h :: _)%list => sx h
                 end)
       (fun t => match t with
                   | nil => True
                   | (h :: _)%list => (tillnow_msg_keeps oof sx ex) h
                 end).

Definition ever_was prop := tillnow prop (fun _ => True).

(** there is at least one message in the trace, that conforms to [event] *)
Definition ever_happened
           (event : w.msg -> Prop) :
  list w.msg -> Prop
  := tillnow_msg
       (fun _ => True)
       event
       (fun _ => False).

(** there was ever a trace, where [state2] was entered, while some time before [state1] was entered *)
Definition after
           (state1 state2 : list w.msg -> Prop)
           l :
  Prop
  := exists s t, (s ++ t)%list = l /\ ever_was state1 t /\ ever_was state2 l.

Definition strictly_after
           state1
           state2 :
  list w.msg -> Prop
  := after (fun t => length t <> 0 /\ state1 t) state2.

End LTLUtil.
